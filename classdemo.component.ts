import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classdemo',
  templateUrl: './classdemo.component.html',
  styleUrls: ['./classdemo.component.css']
})
export class ClassdemoComponent {
  public title = 'Amazon Shoping';
  public selectedTheme = 'electronics';

  public effects = [];
  public addedEffects;


  public selectedEffects = [];

  // tslint:disable-next-line: typedef
  public addEffects() {
    this.effects.push(this.selectedEffects);
  }

  // tslint:disable-next-line: typedef
  public removeEffect() {
    const index = this.effects.indexOf(this.addedEffects, 0);
    if (index > -1) {
      this.effects.splice(index, 1);
    }
  }
}
